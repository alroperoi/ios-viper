//
//  PowersView.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/29/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import UIKit

class PowersView: UIViewController {

    // MARK: Properties
    var presenter: PowersPresenterProtocol?

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
}

extension PowersView: PowersViewProtocol {
    // TODO: implement view output methods
}
