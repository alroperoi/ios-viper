//
//  MenuController.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/23/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import UIKit

class MenuController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var options: UIView!
    @IBOutlet weak var table: UITableView!
    
    // MARK: Properties
    var data : [menuItem] = []
    
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        data.append(menuItem(label: "Home", icon: #imageLiteral(resourceName: "ico-home")))
        data.append(menuItem(label: "Messages", icon: #imageLiteral(resourceName: "ico-messages")))
        data.append(menuItem(label: "My favorites", icon: #imageLiteral(resourceName: "ico-creations")))
        data.append(menuItem(label: "Contacts", icon: #imageLiteral(resourceName: "ico-contacts")))
        data.append(menuItem(label: "Business", icon: #imageLiteral(resourceName: "ico-bussines")))
        data.append(menuItem(label: "Settings", icon: #imageLiteral(resourceName: "ico-settings")))
        data.append(menuItem(label: "Logout", icon: #imageLiteral(resourceName: "ico-logout")))
                        
        table.register(UINib(nibName:"MenuTableViewCell", bundle: Bundle.main),forCellReuseIdentifier: "MenuTableViewCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = data[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        cell.initWithData(label: obj.label, icon: obj.icon)
        cell.selectionStyle = .none
        return cell
    }
}

struct menuItem {
    var label: String
    var icon: UIImage
}
