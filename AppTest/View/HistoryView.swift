//
//  HistoryView.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/29/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import UIKit

class HistoryView: UIViewController {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var historyLabel: UILabel!
    
    // MARK: Properties
    var presenter: HistoryPresenterProtocol?
    var hero: Hero?
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        image.image = hero?.image
        titleLabel.text = hero?.name
        historyLabel.text = hero?.history
    }
}

extension HistoryView: HistoryViewProtocol {
    // TODO: implement view output methods
}
