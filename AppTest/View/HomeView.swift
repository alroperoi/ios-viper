//
//  HomeView.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/23/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import UIKit

class HomeView: BaseController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collection: UICollectionView!
    
    // MARK: Properties
    var presenter: HomePresenterProtocol?
    var heroes: [Hero] = []
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        presenter!.getHeros(callback: {response in
            self.heroes = response
            self.collection.reloadData()
        })
        collection.register(UINib(nibName: "CollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "CollectionViewCell")
        let screenSize = UIScreen.main.bounds.size
        let cellWidth = floor(screenSize.width - 40)
        let cellHeight = floor(screenSize.height * 5 / 6 )
        let insetX = 20
        let insetY = 0
        collection.delegate = self
        collection.dataSource = self
        let layout = collection.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        collection.contentInset = UIEdgeInsets(top: CGFloat(insetY), left: CGFloat(insetX), bottom: CGFloat(insetY), right: CGFloat(insetX))
        
    }
     
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return heroes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let obj = heroes[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        cell.initWithData(title: obj.name, description: obj.description, image: obj.image)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = heroes[indexPath.row]
        presenter!.showHistory(hero: obj)
    }
    
    @IBAction func showMenu(_ sender: UIButton) {
        showMenuController()
    }
}

extension HomeView: HomeViewProtocol {
    // TODO: implement view output methods
}
