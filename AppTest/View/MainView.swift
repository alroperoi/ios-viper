//
//  MainView.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/23/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import UIKit

class MainView: BaseController {

    @IBOutlet weak var UsenameTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    @IBOutlet weak var LoginButton: UIButton!
    @IBOutlet weak var ForgotPasswordButton: UIButton!
    
    // MARK: Properties
    var presenter: MainPresenterProtocol?

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        LoginButton.layer.cornerRadius = 5
    }
    
    @IBAction func showHome(_ sender: UIButton) {
        presenter!.showHome()
    }
}

extension MainView: MainViewProtocol {
    // TODO: implement view output methods
}
