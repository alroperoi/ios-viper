//
//  MainWireFrame.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/23/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import UIKit

class MainWireFrame: MainWireFrameProtocol {

    class func createMainModule() -> UIViewController {
        let navController = mainStoryboard.instantiateViewController(withIdentifier: "MainView")
        if let view = navController as? MainView {//navController.children.first solo para las primeras vistas
            let presenter: MainPresenterProtocol & MainInteractorOutputProtocol = MainPresenter()
            let interactor: MainInteractorInputProtocol = MainInteractor()
            let wireFrame: MainWireFrameProtocol = MainWireFrame()
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            interactor.presenter = presenter
            
            return navController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    func showHome(from view: MainViewProtocol) {
        let new = HomeWireFrame.createHomeModule()
        if let newView = view as? UIViewController{
            newView.navigationController?.pushViewController(new, animated: true)
        }
    }
}
