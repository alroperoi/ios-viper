//
//  HistoryWireFrame.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/29/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import UIKit

class HistoryWireFrame: HistoryWireFrameProtocol {

    class func createHistoryModule(hero: Hero) -> UIViewController {
        let navController = mainStoryboard.instantiateViewController(withIdentifier: "HistoryView")
        if let view = navController as? HistoryView {//navController.children.first solo para las primeras vistas
            let presenter: HistoryPresenterProtocol & HistoryInteractorOutputProtocol = HistoryPresenter()
            let interactor: HistoryInteractorInputProtocol = HistoryInteractor()
            let wireFrame: HistoryWireFrameProtocol = HistoryWireFrame()
            
            view.presenter = presenter
            view.hero = hero
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            interactor.presenter = presenter
            
            return navController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Home", bundle: Bundle.main)
    }
    
}
