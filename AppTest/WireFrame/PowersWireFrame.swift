//
//  PowersWireFrame.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/29/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import UIKit

class PowersWireFrame: PowersWireFrameProtocol {

    class func createPowersModule() -> UIViewController {
        let navController = mainStoryboard.instantiateViewController(withIdentifier: "PowersView")
        if let view = navController as? PowersView {//navController.children.first solo para las primeras vistas
            let presenter: PowersPresenterProtocol & PowersInteractorOutputProtocol = PowersPresenter()
            let interactor: PowersInteractorInputProtocol = PowersInteractor()
            let wireFrame: PowersWireFrameProtocol = PowersWireFrame()
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            interactor.presenter = presenter
            
            return navController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Home", bundle: Bundle.main)
    }
    
}
