//
//  CollectionViewCell.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/29/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var box: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var width: NSLayoutConstraint!
    
    func initWithData(title: String, description: String, image: UIImage){
        self.title.text = title
        self.descriptionLabel.text = description
        self.image.image = image
        box.layer.cornerRadius = 10
        height.constant = UIScreen.main.bounds.height * 5 / 6
        width.constant = UIScreen.main.bounds.width - 40
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

}
