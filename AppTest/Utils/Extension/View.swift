//
//  View.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/29/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    func dropShadowView(width : CGFloat = 0, height: CGFloat = 0, Opacity: Float = 0.2) {
        let Opacity = (Opacity == 0.2) ? 0.2:Opacity
        let height = (height == 0) ? self.bounds.height : height
        let width = (width == 0) ? self.bounds.width : width
        let contactRect = CGRect(x: 0, y: 0, width: width - 35, height: height  )
        layer.shadowPath = UIBezierPath(rect: contactRect).cgPath
        layer.shadowRadius = 10
        layer.shadowOffset = .zero
        layer.shadowOpacity = Opacity
    }
}
