//
//  String.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/29/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    func normalText(size: Int) -> NSMutableAttributedString {
        let attributes: [NSMutableAttributedString.Key: Any] = [
            .font : UIFont(name: "MarkerFelt-Thin", size: CGFloat(size)) as Any,
            .foregroundColor: UIColor.black,
        ]
        
        return NSMutableAttributedString(string: self, attributes: attributes)
    }
    
}
