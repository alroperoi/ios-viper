//
//  HomeInteractor.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/23/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import SwiftyJSON

class HomeInteractor: HomeInteractorInputProtocol {

    // MARK: Properties
    weak var presenter: HomeInteractorOutputProtocol?
    
    func getHeros(callback: @escaping ([Hero])->Void){
        Services.getHeroes(callback: {response in
            var heros : [Hero] = []
            for item in response{
                heros.append(Hero(json: item))
            }
            callback(heros)
        })
    }

}

