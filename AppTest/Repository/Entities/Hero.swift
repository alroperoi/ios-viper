//
//  Hero.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/29/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class Hero {
    
    var id: String
    var name: String
    var description: String
    var history: String
    var image: UIImage
    
    init(
        id: String,
        name: String,
        description: String,
        history: String,
        image: UIImage
    ) {
        self.id = id
        self.name = name
        self.description = description
        self.history = history
        self.image = image
        
    }
    
    init(json: JSON) {
        id = json["id"].stringValue
        name = json["name"].stringValue
        description = json["description"].stringValue
        history = json["history"].stringValue
        image = UIImage(named: json["image"].stringValue)!
    }
}
