//
//  getHeroes.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/29/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import SwiftyJSON

class Services {
    
    static func getHeroes(callback: @escaping ([JSON])->Void){
        let json = JSON([
            [
                "id": "1",
                "name" : "Black Panther",
                "description" : "Fuerza sobrehumana, velocidad, reflejos, agilidad, resistencia, durabilidad y sentidos. Intelecto de nivel de genio, inventor.",
                "history" : """
                Pantera Negra es el título ceremonial otorgado al jefe de la tribu Pantera de la avanzada nación africana de Wakanda. Además de gobernar el país, también es el jefe de sus diversas tribus (colectivamente denominadas Wakandas). El traje de Pantera es un símbolo de la oficina (jefe de estado) y se utiliza incluso durante misiones diplomáticas.

                En un pasado distante, un meteorito hecho de vibranium (ficticio), que absorbe la vibración, se estrelló en Wakanda y fue desenterrado. Razonando que los extranjeros explotarían a Wakanda por este valioso recurso, el gobernante, el rey T'Chaka, al igual que su padre y otras panteras antes que él, ocultó su país del mundo exterior. La primera esposa de T'Chaka, N'Yami, murió mientras estaba de parto con T'Challa, y su segunda esposa, Ramonda, fue hecha prisionera por Anton Pretorius durante una visita a su tierra natal de Sudáfrica, por lo que durante la mayor parte de su infancia T'Challa fue criado solo por su padre. ​T'Chaka fue asesinado por el aventurero Ulysses Klaw en un intento de apoderarse del montículo de vibranium. Con su gente todavía en peligro, un joven T'Challa usó el arma sonora de Klaw contra Klaw y sus hombres, destrozando la mano derecha de Klaw y obligándolo a huir.

                T'Challa era el siguiente en la línea para ser el rey de Wakanda y Pantera Negra, pero hasta que estuvo listo para convertirse en el líder de la nación, su tío S'yan (el hermano menor de T'Chaka) pasó con éxito las pruebas para convertirse en la Pantera Negra. Mientras estaba en su rito de paso en Wakanda, T'Challa conoció y se enamoró de la aparentemente huérfana adolescente Ororo Munroe, quien creció hasta convertirse en el miembro de los X-Men, Tormenta.3​Los dos rompieron su relación debido a su deseo de vengar la muerte de su padre y convertirse en el hombre que podría liderar adecuadamente a Wakanda, pero se verían a lo largo de los años cuando pudieran.

                T'Challa obtuvo el título y los atributos de la Pantera Negra al derrotar a los diversos campeones de las tribus de Wakanda. Uno de sus primeros actos fue disolver y exiliar a Hatut Zeraze, la policía secreta de Wakanda, y su líder, su hermano adoptivo Hunter el Lobo Blanco. Vendió pequeñas porciones de vibranium a instituciones científicas de todo el mundo, acumulando una fortuna que usó para armarse con tecnología avanzada. Más tarde, para mantener la paz, eligió la Dora Milaje ("adorados") de las tribus rivales para que sirviera como su guardia personal y como esposas ceremoniales en entrenamiento. Luego estudió en el extranjero durante un tiempo antes de regresar a su realeza.

                En su primera aparición publicada, el ahora adulto T'Challa invita al equipo de superhéroe estadounidense Los Cuatro Fantásticos a Wakanda, luego ataca e intenta neutralizarlos individualmente para probarse a sí mismo para ver si está listo para enfrentarse a Klaw, quien había reemplazado su mano derecha destrozada con un arma sónica.5​6​Los Cuatro logran reunirse y bloquear a T'Challa en un contraataque del equipo, lo que permite al Rey impresionado retirarse y explicarse para satisfacción del equipo. Después de que el gobernante se explique a los Cuatro, se hacen amigos y ayudan a T'Challa, y él a su vez los ayuda contra el supervillano Psycho-Man.7​T'Challa más tarde se une a los Vengadores,8​comenzando una larga asociación con ese equipo de superhéroes. Primero lucha contra el Hombre-Mono mientras está con el grupo,9​y luego conoce a la cantante estadounidense Monica Lynne,10​con quien se involucra sentimentalmente. Él ayuda a los Vengadores a derrotar a los segundos Hijos de la Serpiente, y luego revela su verdadera identidad en la televisión estadounidense.11​ Se encuentra con Daredevil, y le revela que había deducido la verdadera identidad de Daredevil.
                """,
                "image": "black-panter"
            ],
            [
                "id": "2",
                "name" : "Viuda negra",
                "description" : "Experta táctica, combatiente cuerpo a cuerpo. Envejecimiento lento y sistema inmunológico mejorado. Tiradora experta y dominio de varias otras armas.",
                "history" : """
                Natasha nació en Stalingrado (ahora Volgogrado), Rusia. La primera y más conocida Viuda Negra, es una agente rusa entrenada como espía, artista marcial y francotiradora, y equipada con un arsenal de armas de alta tecnología, que incluye un par de armas energéticas montadas en la muñeca y apodada "Piquete de la Viuda". No usa vestimenta durante sus primeras apariciones, sino simplemente ropa de noche y un velo. Romanova eventualmente se transforma en estadounidense por razones que incluyen su amor por el arquero convertido en superhéroe, Hawkeye.

                Los primeros indicios de la infancia de Natasha Romanova provienen de Ivan Petrovich, quien se presenta como su chofer de mediana edad y confidente en Amazing Adventures de Black Widow de los años setenta. El hombre le cuenta a Matt Murdock cómo una mujer le había otorgado la custodia de la pequeña Natasha justo antes de su muerte durante la Batalla de Stalingrado en otoño de 1942. En consecuencia, se había sentido comprometido a criar a la huérfana como un padre sustituto y finalmente se había entrenada como Espía soviética, ansiosa por ayudar a su patria.2​En otro flashback, ambientado en la ficticia isla de Madripoor en 1941, Petrovich ayuda al Capitán América y al mutante, Logan, quien luego se convertiría en el súper agente canadiense y héroe disfrazado de Wolverine, para rescatar a Natasha de los nazis.

                Una revisada reconectada de su origen, ella establece como ser levantado de la primera infancia por el programa "Operaciones Viuda Negra" de la URSS, en lugar de únicamente por Ivan Petrovich.4​Petrovitch la había llevado al Departamento X, junto con otras huérfanas jóvenes, donde le habían lavado el cerebro y entrenada en combate y espionaje en la instalación encubierta de la "Habitación Roja". Allí, ella está mejorada biotecnológica y psicotecnológicamente, una contabilidad que proporciona una base racional para su vida útil inusualmente larga y juvenil. Durante ese tiempo ella tuvo algún entrenamiento bajo con el Soldado del Invierno, y la pareja incluso tuvo un breve romance.5​Cada Viuda Negra se despliega con recuerdos falsos para ayudar a asegurar su lealtad. Romanova eventualmente descubre esto, incluido el hecho de que nunca, como ella había creído, había sido una bailarina. Además, descubre que la Habitación Roja todavía está activa como "2R".

                Natasha fue organizada por la KGB para casarse con el famoso piloto de pruebas soviético Alexei Shostakov. Sin embargo, cuando el gobierno soviético decidió convertir a Alexei en su nuevo operativo, el Guardián Rojo, se le dice que no puede tener más contacto con su esposa. Le dicen a Natasha que murió y que está entrenado como agente secreto por separado.
                """,
                "image": "black-widow"
            ],
            [
                "id": "3",
                "name" : "Bruja escarlata",
                "description" : "Poderes Psíquicos, Telepatía, Teletransportación, Manipulación de la energía, Desintegración, Distorsión de la realidad, Magia del caos, Telequinesis, Vuelo, Fragoquinesis",
                "history" : """
                Después de la trágica muerte de su hija, Anya, en un incendio provocado por una multitud antimutante, el mutante Magneto, utilizó sus poderes y exterminó a toda la multitud ante la mirada horrorizada de su esposa, Magda. Magda huyó de su marido hacia la pequeña Nación de Transia en los Montes Balcanes, estando ya embarazada de mellizos. Magda dio a luz en la Montaña Wundagore, "La ciudadela del Alto Evolucionador", a los mellizos Wanda y Pietro. Ese día, el dios antiguo caído conocido como Chton le infundió a la recién nacida Wanda un gran potencial mágico además de sus habilidades mutantes. Magda murió luego del parto.

                Por ese entonces también se presentaron en Wundagore Robert y Madeline Frank, viejos superhéroes conocidos como Whizzer y Miss América, quienes estaban esperando un hijo. Su hijo nació muerto, y Madeline murió con él. Bova, la mujer-vaca, asistente del Alto Evolucionador, ayuda a Madeline en la labor de parto. Madeline pierde la vida en el parto, de la misma manera que lo hace el hijo que esperaba. Bova decidió arreglar las cosas dándole los mellizos a Robert como si fueran sus hijos, pero al saber de la muerte de su esposa éste escapó.2​ Entonces, el Alto Evolucionador se presentó como un dios ante el gitano Django Maximoff y su esposa Marya y les entregó a los niños, que fueron entonces nombrados como Pietro y Wanda.

                Después de que Django Maximoff robara pan de una ciudad vecina con el fin de alimentar a su famélica familia, los ciudadanos incendiaron la aldea romaní, matando a Marya. Pietro llevó a Wanda a un lugar seguro y los dos se mantuvieron deambulando por Europa. Los eventos de su infancia fueron tan traumáticos que no los recordaron hasta bien entrada la vida adulta. Después de que Wanda usó sus poderes para salvar a un niño, fueron perseguidos por una muchedumbre y salvados por Magneto, (sin que ninguno conociera el lazo sanguíneo que los unía). Como consecuencia, Magneto los recluta para formar parte de la Hermandad de Mutantes Diabólicos, que luchó contra los X-Men en varias ocasiones. Cuando Magneto fue secuestrado por la entidad cósmica Stranger, la Hermandad se disuelve y los gemelos declaran que su deuda con Magneto ha sido pagada.
                """,
                "image": "bruja"
            ],
            [
                "id": "4",
                "name" : "Capitan América",
                "description" : "Sentidos, agilidad, velocidad y fuerza sobrehumanas. Gran resistencia, inmune a gases y enfermedades, curación y regeneración acelerada.",
                "history" : """
                Capitán América cuyo nombre real es Steven "Steve" Grant Rogers, es un superhéroe ficticio que aparece en los cómics estadounidenses publicados por Marvel Comics. Creado por los historietistas Joe Simon y Jack Kirby, el personaje apareció por primera vez en Captain America Comics #1 (marzo de 1941) de Timely Comics, predecesor de Marvel Comics. El Capitán América fue diseñado como un supersoldado patriota que luchaba frecuentemente contra las potencias del Eje en la Segunda Guerra Mundial, y fue el personaje más popular de Timely Comics durante el período de guerra. La popularidad de los superhéroes se desvaneció después de la guerra, y el cómic del Capitán América dejó de editarse en 1950, con un breve resurgimiento en 1953. Desde que Marvel Comics revivió al personaje en 1964, el Capitán América se ha mantenido en publicación.

                El Capitán América viste un traje que lleva un motivo de la bandera de los Estados Unidos, y utiliza un escudo casi indestructible que lanza como proyectil. El personaje es generalmente representado como el álter ego de Steve Rogers, un joven frágil mejorado a la cima de la perfección humana por un suero experimental S.S.S. (Suero supersoldado) para ayudar a los esfuerzos inminentes del gobierno de Estados Unidos en la Segunda Guerra Mundial. Cerca del final de la guerra, queda atrapado en el hielo y sobrevive en animación suspendida hasta que es descongelado en el presente. A pesar de que el Capitán América a menudo lucha por mantener sus ideales como un hombre fuera de su tiempo con sus realidades modernas, sigue siendo una figura muy respetada en su comunidad, hasta convertirse en el líder de Los Vengadores.
                """
                ,
                "image": "captain-america"
            ],
            [
                "id": "5",
                "name" : "Dr. Strange",
                "description" : "Reputado médico en neurocirugía. Control de la magia. Esperanza de vida prolongada por el Ankh de la vida. Dueño del Ojo de Agamotto",
                "history" : """
                Stephen Strange es un médico especializado en neurocirugía, codicioso y egocéntrico, que solo se preocupa por la riqueza de su carrera, hasta que en un accidente sufrió una enfermedad nerviosa en sus manos que le obligó a retirarse. Cuando su padre murió, su hermano fue a visitarlo para recriminarle que no fue al funeral ese día. Stephen estaba con una chica, por lo que su hermano salió enfadado. Estaba nevando esa noche y hubo un accidente en el cual su hermano murió al ser atropellado. Stephen hizo que su cadáver fuera criogenizado hasta el día en que la ciencia lo pudiera revivir.

                Un día escuchó hablar en un puerto acerca de un tibetano con poderes, por lo que fue a verle y con el aprendió las artes místicas, ayudó a su mentor "el Anciano", quien poseía el título de Hechicero Supremo de esta dimensión, a repeler todo el mal místico que quiera causar daño a esta dimensión; tuvo enfrentamientos con sus enemigos, algunos traidores como el Barón Mordo quien deseaba el título de hechicero supremo, y consiguió vencer a entidades místicas extra-dimensionales como Pesadilla, Dormammu, entre otros.

                Desde la primera historia la residencia de Strange, el Sanctum Sanctorum, era una parte de la mitología del personaje, caracterizada por la ventana circular dividida por tres líneas de barrido en la parte frontal de la residencia (en realidad es el sello de protección de la Vishanti). El sirviente personal de Strange, Wong, custodiaba la residencia en su ausencia.

                En muchas ocasiones ayudó a otros superhéroes como los 4 Fantásticos, se encontró con el dios nórdico Thor, y el hermano adoptivo Loki, y apoyó a Spider-Man. En instantes tuvo encuentros con nuevas entidades cósmicas y místicas como el "tribunal viviente" y "Umar" la hermana de Dormammu.

                Decidió tener una identidad secreta y un nuevo traje aunque similar al anterior pero que incluyera una máscara unida a la vestimenta que cubriera toda la cabeza.

                Ayudó a los X-Men a vencer a Juggernaut y apareció en cómics posteriores junto a Namor y Hulk con quienes decidió formar un equipo llamado Los Defensores, posteriormente Silver Surfer se incorporaría al equipo. Entre sus batallas, de las más resaltadas, esta cuando la entidad conocida como Shuma Gorath amenazaba con entrar en nuestra realidad a través de la mente de "el Anciano", Strange se vio obligado a cerrar la mente del Anciano, causando la muerte física de su mentor, sin embargo, aseguró que era un sacrificio necesario y su alma se fusionó con la Eternidad una entidad cósmica. Con la muerte del venerable anciano, Dr Strange asumió el título de Hechicero Supremo y regresó a su vestimenta original.

                Strange perdió el título de "El Hechicero Supremo" cuando se negó a luchar en una guerra en nombre de la Vishanti, las entidades místicas que permiten sus hechizos. Durante este tiempo pasó a formar parte de los "Hijos de medianoche", un grupo de personajes sobrenaturales, y Strange debe encontrar nuevas fuentes de fuerza mágica de caos. También formaría a los defensores secretos: Spiderman, Spiderwoman, Wolverine, Hulk y Ghost Rider, para después volver a reunirse con los defensores originales y recuperar su título cuando terminó la guerra a la cual había declinado.

                Después de que los equipos se deshicieran ayudó en ocasiones a otros héroes como los miembros de los 4 fantásticos, Ben Grimm y a la Antorcha humana, y paso a ser un personaje secundario en otros cómics.

                Strange apareció como un personaje secundario de la década de 2000 con Los Nuevos Vengadores, y formó un consejo secreto llamado los Illuminati para hacer frente a futuras amenazas a la Tierra. En la historia que implica la introducción de una ley federal de Registro de Superhumano que divide la comunidad de superhéroes, la conocida serie de cómics Civil War, Strange se opone a la inscripción de oficio, aunque no toma parte activa en el conflicto, más allá de como se menciona en los cómics siguientes a las 7 publicaciones, según su ayudante y asentido por él mismo, podía haber detenido fácilmente la contienda, cuando esta se encontraba en su peor momento. Pasa a ser miembro de los Nuevos Vengadores que en secreto los refugia en su residencia el Sanctum Sanctorum bajo una ilusión haciendo parecer que está abandonado. La legislación fue derogada finalmente.

                Strange buscó a un sucesor Hechicero Supremo después de que él había considerado que abusó de la magia negra para salvar a los Nuevos Vengadores de la muerte en un enfrentamiento con un equipo de criminales. Decide nombrar Hechicero Supremo a Brother Voodoo quien se sacrifica con el fin de detener la poderosa entidad mística Agamotto de recuperar el ojo. El tema siguiente, un sentimiento de culpa lo obliga a dejar a los Nuevos Vengadores, pero Luke Cage lo convence de quedarse, así que ofrece al equipo de su siervo Wong para que actúe como su ama de llaves.

                Los Illuminati son reunidos nuevamente, esta vez por T'Challa (Pantera Negra), al ver una incursión de otro planeta Tierra que fue destruido en un universo diferente, por lo que descubren que cada ocho horas una nueva incursión amenaza con destruir los universos correspondientes si una de las dos Tierras en incursión no es destruida, colocando al Dr. Strange y al resto del equipo en la decisión de destruir los planetas en incursión, abandonando la moralidad y la humanidad.
                """,
                "image": "dr-strange"
            ],
            [
                "id": "6",
                "name" : "Gamora",
                "description" : "Fuerza sobrehumana, agilidad sobrehumana, factor de curación acelerado, artista marcial experta",
                "history" : """
                Gamora es la última de su especie, los Zen Whoberi, quienes fueron exterminados por los Badoon (en su línea temporal original, su especie fue exterminada por la Iglesia Universal de la Verdad). Thanos encontró a la niña y decidió utilizarla como un arma. Gamora fue criada y entrenada por Thanos para asesinar a Magus, una malvada versión alterna de Adam Warlock. Thanos mostró su poca humanidad en su infancia, pero Gamora fue muy leal al hombre que le prometió la oportunidad de vengar la muerte de su familia. Gamora pronto dominó las artes marciales, ganándose el apodo de "la mujer más letal en toda la galaxia". Cuando ella era una adolescente, Thanos la llevó en un viaje. Gamora desobedeció las órdenes de Thanos, y debido a esto entró en conflicto con unos maleantes. Ella fue superada enormemente en número, y a pesar de sus habilidades fue derrotada y luego violada por uno de los agresores. Thanos la encontró medio muerta, y a su vez asesinó a todos los agresores y le devolvió la salud, mejorando cibernéticamente sus habilidades a niveles sobrehumanos.

                De adulta, Gamora fue enviada como una asesina contra la Iglesia Universal de la Verdad, siendo rápidamente temida por sus agentes, los Caballeros Negros. Ella se vengó por el genocidio de su raza, asesinando a todos los miembros de la Iglesia involucrados antes de que realmente ocurriera el acontecimiento. Gamora conoció y se unió a Adam Warlock, quien quería detener a su vieja versión.2​3​ Ella incluso consiguió estar cerca de Magus, pero finalmente falló en asesinarlo. Junto con Warlock, Pip el Troll y Thanos, Gamora luchó para escapar de los Caballeros Negros de la Iglesia Universal de la Verdad y el Escuadrón de la Muerte de Magus.4​5​6​Más tarde fue asignada por Thanos para proteger a Adam Warlock, pero ella comenzó a sospechar de los planes de Thanos, para después ser atacada por Drax el Destructor.7

                Finalmente, Magus fue derrotado, pero Thanos se reveló a sí mismo como una amenaza mucho mayor. Gamora ayudó al Capitán Mar-Vell, Drax, y a los Vengadores contra Thanos. Ella y Pip trataron de impedir que Thanos destruyera toda la vida en el universo. Gamora intentó matar a Thanos, pero él la hirió mortalmente, y destruyó la mente de Pip.8​Adam Warlock los encontró, y Gamora le advirtió a Adam de los planes de Thanos. A continuación, Warlock absorbió su alma dentro de la Gema del Alma. Cuando Adam Warlock también murió, su espíritu se reunió con el de sus amigos en el "Mundo de Almas", dentro de la Gema.
                """,
                "image": "gamora"
            ],
            [
                "id": "7",
                "name" : "Iron Man",
                "description" : "Fuerza sobrehumana y durabilidad. Vuelo supersónico. Repulsor de energía y misiles de proyección. Regenerativo soporte vital.",
                "history" : """
                Iron Man (conocido en español como El Hombre de Hierro) es un superhéroe ficticio que aparece en los cómics estadounidenses publicados por Marvel Comics. El personaje fue cocreado por el escritor y editor Stan Lee, desarrollado por el guionista Larry Lieber y diseñado por los artistas Don Heck y Jack Kirby. Hizo su primera aparición en Tales of Suspense #39 (marzo de 1963), y recibió su propio título en Iron Man #1 (mayo de 1968).

                Anthony Edward Stark, conocido como Tony Stark, un multimillonario magnate empresarial estadounidense, playboy e ingenioso científico, quien sufre una grave lesión en el pecho durante un secuestro. Cuando sus captores intentan forzarlo a construir un arma de destrucción masiva crea, en cambio, una armadura poderosa para salvar su vida y escapar del cautiverio. Más tarde, Stark desarrolla su traje, agregando armas y otros dispositivos tecnológicos que diseñó a través de su compañía, Industrias Stark. Él usa el traje y las versiones sucesivas para proteger al mundo como Iron Man. Aunque al principio ocultó su verdadera identidad, Stark finalmente declaró que era, de hecho, Iron Man en un anuncio público.

                Inicialmente, Iron Man no era más que un concepto de Stan Lee con el fin de explorar los temas de la Guerra Fría, particularmente el papel de la tecnología y la industria estadounidenses en la lucha contra el comunismo.2​ Las re imaginaciones posteriores de Iron Man han pasado de los motivos de la Guerra Fría a los asuntos contemporáneos de la época,2​ como lo es el terrorismo, la corrupción y la delincuencia en general.

                Durante la mayor parte de la historia de la publicación del personaje, Iron Man fue miembro y líder del equipo Los Vengadores, ha aparecido en varias encarnaciones de sus diversas series de cómics y también fue adaptado para varios programas de televisión y películas animadas.
                """,
                "image": "iron-man"
            ],
            [
                "id": "8",
                "name" : "Loki",
                "description" : "Inteligencia sobrehumana, fuerza, longevidad, magia que incluye proyecciones astrales, descargas de energía, vuelo, teleportación dimensional y telepatía. Además ilusionista y cambia su aspecto físico a voluntad.",
                "history" : """
                Hace muchos años, Bor, gobernante de Asgard, estaba luchando contra los Gigantes de hielo, siguió a un gigante herido hasta un poderoso hechicero que lo estaba esperando. El hechicero lo atrapó sin darse cuenta, convirtiendo a Bor en nieve. Al maldecir a su hijo, Odín, llevó a los Asgardianos a la batalla contra los Gigantes de Escarcha y mató a Laufey, quien era el rey, en combate personal. Odín encontró un pequeño niño del tamaño de un Asgardiano, escondido dentro de la fortaleza principal de los Gigantes de hielo. El niño era Loki y Laufey lo había mantenido oculto a su gente debido a su vergüenza por el tamaño pequeño de su hijo. Odín tomó al niño, por una combinación de lástima, para apaciguar a su padre, y porque él era el hijo de un digno adversario asesinado en un combate honorable, y lo crio como su hijo junto a su hijo biológico thor, loki es considerado el dios de la trampa y de las mentiras, sus habilidades son: teletransportación, teleqinesis, cambio de forma, y la más poderosa, el poder de hipnotizar a la gente a su gusto para obligarlos a hacer cosas malas y macabras para lograr sus metas. (Mayo, Junio 2008)</ref>

                A lo largo de su infancia y en la adolescencia, Loki estaba resentido por las diferencias en las que él y Thor fueron tratados por los ciudadanos de Asgard. Los asgardianos valoraban la gran fuerza, tenacidad y valentía en la batalla por encima de todas las cosas, y Loki era claramente inferior a su hermano Thor en esas áreas. Lo que le faltaba en tamaño y fuerza, sin embargo, lo compensaba en poder y habilidad, particularmente como hechicero. A medida que Loki creció hasta la edad adulta, su talento natural para causar travesuras se haría manifiesto y le valió un apodo como el "Dios de las mentiras y las travesuras"; su maldad con el tiempo se convirtió en malicia a medida que su hambre de poder y venganza se hizo más fuerte. Varias veces intentó usar trucos para deshacerse de Thor, como decirle que guarde un agujero en la pared que había hecho. Con el tiempo, su reputación pasó de ser un tramposo juguetón y travieso al "Dios del Mal". A lo largo de los siglos, Loki intentó en muchas ocasiones tomar el reinado de Asgard y destruir a Thor. Incluso ayudó al gigante Tormenta Ghan a escapar de Thor planeando obtener una deuda de él más tarde, y ayudó a otros enemigos de Asgard, planeando tomar Odin, que se había cansado de las travesuras de Loki, lo aprisionó mágicamente dentro de un árbol hasta que alguien derramó una lágrima por él. Loki finalmente se liberó haciendo que una hoja golpeara a Heimdall, el guardián del Bifrost, en el ojo, lo que le hizo derramar una lágrima. Loki compiló un extenso historial criminal en Asgard, y con frecuencia fue exiliado.7​Conoció al hechicero Eldred, quien le enseñó magia negra. Le pagó a Eldred y luego se lo entregó al demonio Fuego Surtur.
                """,
                "image": "loki"
            ],
            [
                "id": "9",
                "name" : "Hombre araña",
                "description" : "Nivel intelecto de Genio. Competente científico e inventor. Fuerza sobrehumana, velocidad, durabilidad, agilidad, resistencia, reflejos, coordinación, equilibrio y resistencia.",
                "history" : """
                Spider-Man (llamado Hombre Araña en muchas de las traducciones al español) es un superhéroe ficticio creado por los escritores y editores Stan Lee y Steve Ditko. Apareció por primera vez en el cómic de antología Amazing Fantasy # 15 (10 de agosto de 1962), en la Edad de Plata de los cómics. Aparece en los cómics estadounidenses publicados por Marvel Comics, así como en varias películas, programas de televisión y adaptaciones de videojuegos ambientadas en el Universo Marvel. En las historias, Spider-Man es el alias de Peter Parker, un huérfano criado por su tía May y su tío Ben en la Ciudad de Nueva York después de que sus padres Richard y Mary Parker murieron en un accidente aéreo. Lee y Ditko tuvieron que lidiar con los problemas de la adolescencia y los problemas financieros, y lo acompañaron con muchos personajes de apoyo, como J. Jonah Jameson, Flash Thompson, Harry Osborn, los intereses románticos, Gwen Stacy y Mary Jane Watson, y enemigos como el Doctor Octopus, Kingpin, Duende Verde y Venom. Su historia de origen lo tiene adquiriendo habilidades relacionadas con la araña después de un mordisco de una araña radioactiva; estos incluyen aferrarse a las superficies, disparar telarañas desde dispositivos montados en la muñeca y detectar el peligro con su "sentido arácnido".

                Cuando Spider-Man apareció por primera vez a principios de la década de 1960, los adolescentes en cómics de superhéroes solían ser relegados al papel de acompañante del protagonista. La serie de Spider-Man abrió camino al presentar a Peter Parker, un estudiante de preparatoria de Queens detrás de la identidad secreta de Spider-Man y con cuyas "auto-obsesiones con el rechazo, la inadecuación y la soledad" podrían relacionar los lectores jóvenes.11​Mientras que Spider-Man tenía todas las características de un compañero, a diferencia de los héroes adolescentes anteriores como Bucky y Robin, Spider-Man no tenía un mentor de superhéroes como el Capitán América y Batman; por lo tanto, tuvo que aprender por sí mismo que "con un gran poder también debe llevar una gran responsabilidad", una línea incluida en un cuadro de texto en el panel final de la primera historia de Spider-Man, pero más tarde atribuida de manera retroactiva a su tutor, el difunto tío Ben.

                Marvel ha presentado a Spider-Man en varias series de cómics, la primera y más duradera de las cuales es The Amazing Spider-Man. A lo largo de los años, el personaje de Peter Parker se desarrolló desde un tímido y nerd estudiante de secundaria de la ciudad de Nueva York hasta un estudiante universitario preocupado pero extrovertido, hasta un maestro de secundaria casado para, a fines de la década de 2000, un único fotógrafo independiente. En la década de 2010, se une a los Vengadores, el equipo estrella de superhéroes de Marvel. El némesis de Spider-Man, el Doctor Octopus, también tomó la identidad de un arco de historia que abarca el 2012-2014 llamado "Superior Spider-Man", luego de una trama de intercambio de cuerpos en la que Peter parece morir.12​Marvel también ha publicado libros con versiones alternativas de Spider-Man, incluido Spider-Man 2099, que presenta las aventuras de Miguel O'Hara, el Spider-Man del futuro; Ultimate Spider-Man, que presenta las aventuras de un adolescente Peter Parker en un universo alternativo; y Ultimate Comics Spider-Man, que representa al adolescente Miles Morales, quien toma el manto de Spider-Man después de la supuesta muerte de Ultimate Peter Parker. Miles luego es llevado a la continuidad de la corriente principal, donde trabaja junto a Peter.
                """,
                "image": "spider-man"
            ],
            [
                "id": "10",
                "name" : "Thor",
                "description" : "Fuerza sobrehumana, velocidad, durabilidad, manipulación del trueno.",
                "history" : """
                Thor (Thor Odinson) es un superhéroe ficticio que aparece en los cómics estadounidenses publicados por Marvel Comics. El personaje, que se basa en la deidad nórdica del mismo nombre, es el dios del trueno asgardiano poseedor del martillo encantado, Mjolnir, que le otorga la capacidad de volar y manipular el clima entre sus otros atributos sobrehumanos. 1

                Debutando en la Edad de Plata de los Libros de Cómics, el personaje apareció por primera vez en Journey into Mystery # 83 (agosto de 1962) y fue creado por el dibujante Jack Kirby, el editor Stan Lee y el guionista Larry Lieber. Ha protagonizado varias series en curso y series limitadas, y es miembro fundador del equipo de superhéroes, Los Vengadores, apareciendo en diferentes números de esa serie. El personaje también ha en diversos productos de Marvel, incluidas series de televisión animadas, películas, videojuegos, ropa, juguetes y tarjetas de intercambio.
                
                """,
                "image": "thor"
            ]
        ])
        callback(json.arrayValue)
    }
    
    
}
