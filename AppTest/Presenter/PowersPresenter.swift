//
//  PowersPresenter.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/29/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation

class PowersPresenter  {
    
    // MARK: Properties
    weak var view: PowersViewProtocol?
    var interactor: PowersInteractorInputProtocol?
    var wireFrame: PowersWireFrameProtocol?
    
}

extension PowersPresenter: PowersPresenterProtocol {
    // TODO: implement presenter methods
    func viewDidLoad() {
    }
}

extension PowersPresenter: PowersInteractorOutputProtocol {
    // TODO: implement interactor output methods
}
