//
//  MainPresenter.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/23/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation

class MainPresenter  {
    
    // MARK: Properties
    weak var view: MainViewProtocol?
    var interactor: MainInteractorInputProtocol?
    var wireFrame: MainWireFrameProtocol?
    
    func showHome(){
        wireFrame!.showHome(from: view!)
    }
}

extension MainPresenter: MainPresenterProtocol {
    // TODO: implement presenter methods
    func viewDidLoad() {
    }
}

extension MainPresenter: MainInteractorOutputProtocol {
    // TODO: implement interactor output methods
}
