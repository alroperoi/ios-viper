//
//  HistoryPresenter.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/29/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation

class HistoryPresenter  {
    
    // MARK: Properties
    weak var view: HistoryViewProtocol?
    var interactor: HistoryInteractorInputProtocol?
    var wireFrame: HistoryWireFrameProtocol?
    
}

extension HistoryPresenter: HistoryPresenterProtocol {
    // TODO: implement presenter methods
    func viewDidLoad() {
    }
}

extension HistoryPresenter: HistoryInteractorOutputProtocol {
    // TODO: implement interactor output methods
}
