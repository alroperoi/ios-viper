//
//  HomePresenter.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/23/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation

class HomePresenter  {
    
    // MARK: Properties
    weak var view: HomeViewProtocol?
    var interactor: HomeInteractorInputProtocol?
    var wireFrame: HomeWireFrameProtocol?
    
    func getHeros(callback: @escaping ([Hero])->Void){
        interactor!.getHeros(callback: callback)
    }
    
    func showHistory(hero: Hero){
        wireFrame!.showHistory(hero: hero, from: view!)
    }
}

extension HomePresenter: HomePresenterProtocol {
    // TODO: implement presenter methods
    func viewDidLoad() {
    }
}

extension HomePresenter: HomeInteractorOutputProtocol {
    // TODO: implement interactor output methods
}
