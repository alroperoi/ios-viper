//
//  PowersProtocols.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/29/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import UIKit

protocol PowersViewProtocol: class {
    // PRESENTER -> VIEW
    var presenter: PowersPresenterProtocol? { get set }
}

protocol PowersWireFrameProtocol: class {
    // PRESENTER -> WIREFRAME
    static func createPowersModule() -> UIViewController
}

protocol PowersPresenterProtocol: class {
    // VIEW -> PRESENTER
    var view: PowersViewProtocol? { get set }
    var interactor: PowersInteractorInputProtocol? { get set }
    var wireFrame: PowersWireFrameProtocol? { get set }
    
    func viewDidLoad()
}

protocol PowersInteractorOutputProtocol: class {
// INTERACTOR -> PRESENTER
}

protocol PowersInteractorInputProtocol: class {
    // PRESENTER -> INTERACTOR
    var presenter: PowersInteractorOutputProtocol? { get set }
}



