//
//  HistoryProtocols.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/29/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import UIKit

protocol HistoryViewProtocol: class {
    // PRESENTER -> VIEW
    var presenter: HistoryPresenterProtocol? { get set }
}

protocol HistoryWireFrameProtocol: class {
    // PRESENTER -> WIREFRAME
    static func createHistoryModule(hero: Hero) -> UIViewController
}

protocol HistoryPresenterProtocol: class {
    // VIEW -> PRESENTER
    var view: HistoryViewProtocol? { get set }
    var interactor: HistoryInteractorInputProtocol? { get set }
    var wireFrame: HistoryWireFrameProtocol? { get set }
    
    func viewDidLoad()
}

protocol HistoryInteractorOutputProtocol: class {
// INTERACTOR -> PRESENTER
}

protocol HistoryInteractorInputProtocol: class {
    // PRESENTER -> INTERACTOR
    var presenter: HistoryInteractorOutputProtocol? { get set }
}



