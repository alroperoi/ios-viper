//
//  MainProtocols.swift
//  AppTest
//
//  Created by Arlin Ropero on 8/23/20.
//  Copyright © 2020 Proximate. All rights reserved.
//

import Foundation
import UIKit

protocol MainViewProtocol: class {
    // PRESENTER -> VIEW
    var presenter: MainPresenterProtocol? { get set }
    
}

protocol MainWireFrameProtocol: class {
    // PRESENTER -> WIREFRAME
    static func createMainModule() -> UIViewController
    func showHome(from view: MainViewProtocol)
}

protocol MainPresenterProtocol: class {
    // VIEW -> PRESENTER
    var view: MainViewProtocol? { get set }
    var interactor: MainInteractorInputProtocol? { get set }
    var wireFrame: MainWireFrameProtocol? { get set }
    
    func viewDidLoad()
    func showHome()
}

protocol MainInteractorOutputProtocol: class {
// INTERACTOR -> PRESENTER
}

protocol MainInteractorInputProtocol: class {
    // PRESENTER -> INTERACTOR
    var presenter: MainInteractorOutputProtocol? { get set }
}



